%Identifying Coherence Patterns in Bing Brunton's data from
%https://www.bingbrunton.com/data
%Devon J Griggs, working with Julien Bloch
%Fall 2018    

%Uses edfread() downloaded from 
%https://www.mathworks.com/matlabcentral/fileexchange/31900-edfread

%TODO incorporate the grid locations and start looking for patterns

%prepare the parallel workers
%if the numbers printed out of order, then the parfor loop worked correctly
parfor n=1:9,disp(num2str(n)),end

%setup
tic
clearvars -except tic
close all

%PARAMETERS
printtext=true;
% % % % % % % % % % % % % minfreq=1;%Hz
% % % % % % % % % % % % % maxfreq=100;%Hz
% % % % % % % % % % % % % bandwidth=52;%Hz. Note that bandwidth>stepfreq allows overlap of freq ranges
% % % % % % % % % % % % % stepfreq=51;%Hz
folder='C:\Users\Yazdan Lab\Documents\MATLAB\Devon\BingData\c9';

%display different frequencies to be used
disp('Selected frequency ranges')
% % % % % % % % % % % % freql_all=minfreq:stepfreq:maxfreq;
freq_all=[4,7;8,12;12,30;30,100;100,200];
for freqn=1:size(freq_all,1)
    freql=freq_all(freqn,1);
    freqh=freq_all(freqn,2);
    if printtext
        disp(['Freq range: ',num2str(freql),' - ',num2str(freqh),' Hz'])
    end
end

%navigate to the folder
cd(folder);

%iterate through the .edf files
ls=dir();
msc_all_files=cell(size(ls));
% parfor filenum=1:length(ls)
for filenum=1:length(ls)
    if isempty(strfind(ls(filenum).name,'.edf'))
        continue
    end
    filename=ls(filenum).name;
    disp(['Running file: ',filename])
    
    %prep example variables to get appropriate variable sizes
    %ex_header will be saved for the entire file
    [ex_header,ex_data]=edfread(filename,'targetSignals',1);
    filtered_data=zeros(size(ex_data));
    
    msc=cell([size(ex_header.frequency,2),size(ex_header.frequency,2),length(freq_all)]);
    
    %iterate through frequency bands
    for freqn=1:size(freq_all,1)
        freql=freq_all(freqn,1);
        freqh=freq_all(freqn,2);
        disp(['File ',filename,' Freq band: ',num2str(freql),' - ',num2str(freqh)])
        filtered_data=zeros(size(ex_data));
        mscf=cell(size(ex_header.frequency,2));
        
        %calculate the mean square coherence between each pair of electrodes        
        %first iteration through electrodes
%         for e1=1:size(ex_header.frequency,2)-1
        for e1=1:3
            [header1,data1]=edfread(filename,'targetSignals',e1);
            filtered_data1=zeros(size(data1));
            normalizer=header1.frequency/2;
            msc_temp=cell(size(ex_header.frequency,2),1);
            
            %if the data can be filtered, do so. If not, leave a NaN in the
            %first element to notify the later code.
            if freqh/normalizer(e1)<1
                [b,a]=butter(4,[freql/normalizer(e1),freqh/normalizer(e1)],'bandpass');
                filtered_data1=filter(b,a,data1);
            else
                filtered_data1(1)=NaN;
            end
            
            %second iteration through electrodes
%             for e2=(e1+1):size(ex_header.frequency,2)
            for e2=(e1+1):(e1+3)
                disp(['File ',filename,' Freq band: ',num2str(freql),' - ',num2str(freqh),' Electrode numbers ',num2str(e1),',',num2str(e2)])
                [header2,data2]=edfread(filename,'targetSignals',e2);
                filtered_data2=zeros(size(data2));
                normalizer=header2.frequency/2;
                msc_temp=cell(size(ex_header.frequency,2),1);

                %if the data can be filtered, do so. If not, leave a NaN in the
                %first element to notify the later code.
                if freqh/normalizer(e2)<1
                    [b,a]=butter(4,[freql/normalizer(e2),freqh/normalizer(e2)],'bandpass');
                    filtered_data2=filter(b,a,data2);
                else
                    filtered_data2(1)=NaN;
                end
            
                if ~isequal(header1.frequency,header2.frequency)
                    if printtext
                        disp('Frequencies are not equal. Skipping msc for:')
                        disp(['Channel pair ',num2str(e1),'(',num2str(header1.frequency),') - ',num2str(e2),'(',num2str(header2.frequency),')'])
                    end
                    msc_temp{e2}='Frequencies are not equal';
                    continue
                end
                if isnan(filtered_data1(1))||isnan(filtered_data2(1))
                    if printtext
                        disp('Data is not filtered. Skipping msc for:')
%                         disp(['Channel pair ',num2str(e1),'(',num2str(header.frequency(e1)),') - ',num2str(e2),'(',num2str(header.frequency(e2)),')'])
                        disp(['Channel pair ',num2str(e1),'(',num2str(header1.frequency(e1)),') - ',num2str(e2),'(',num2str(header2.frequency(e2)),')'])
                    end
                    msc_temp{e2}='Data is not filtered';
                    continue
                end
                msc_temp{e2}=mscohere(filtered_data1,filtered_data2);
            end%inner, e2 loop
            mscf(e1,:)=msc_temp;
        end%outer, e1 loop
        msc(:,:,freqn)=mscf;
    end%frequency band loop
    msc_struct = struct();
    msc_struct.filename=filename;
    msc_struct.header=ex_header;
    msc_struct.msc=msc;
    msc_all_files{filenum}=msc_struct;
end%file loop

% %delete empty cells ()
% msc_all_files=msc_all_files(~cellfun('isempty',msc_all_files));

%save data
save('msc_all_files.m','msc_all_files');

%end program
toc

%delete empty cells (reference only, not for script)
% msc_all_files=msc_all_files(~cellfun('isempty',msc_all_files));