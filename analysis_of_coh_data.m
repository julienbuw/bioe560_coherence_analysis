%analysis_of_coh_data.m
%Devon Griggs
%BIOEN 560 final project with Julien
clearvars, close all
cd('C:\Users\Yazdan Lab\Documents\MATLAB\Devon\BingData\c9');

%booleans
one_day_only=false;
plot_example=true;
plot_surf=false;
iterate_thru_channels=true;

%loop through the files
coh_ex=figure;
subplotnum=1;
ls=dir();
v=nan(10*495936,3);
f_bin_names={'Delta','Theta','Alpha','Beta','Gamma'};
f_low=[0 4 8  12 30];
f_hi =[4 8 12 30 55];
slope=zeros(7,length(f_hi));%(freq, day)
yintercept=zeros(7,length(f_hi));%(freq, day)
datamean=zeros(7,length(f_hi));%(freq, day)
data_collection_counter = 0;
counter_file_start = 0;
for filenum=1:length(ls)
    counter_file_start=data_collection_counter+1;
    filename=ls(filenum).name;
    if length(filename)<4,continue,end%files names have at least 4 chars
    ext=filename(end-2:end);
    switch ext
        case 'mat'
            %proceed
            disp(['Processing file: ',filename])
        otherwise
            %skip these files
            continue
    end
    daynum=filename(end-4);
    %load in the grid locations
    temp=cd('C:\Users\Yazdan Lab\Documents\MATLAB\Devon\BingData\page4\grid_loc\grid_loc\c95c1e82');
    load('trodes.mat');%UNITS IN MM
    cd(temp);
    max_electrode=size(Grid,1);
    %load file contents into variable sf
    load(filename);
    d=zeros(size(sf.Cxy));
    if iterate_thru_channels
        %calculate distances
        for ch1=1:max_electrode-1
            for ch2=ch1+1:max_electrode
                d(ch1,ch2)=mydist(ch1,ch2,Grid);
            end
        end
        %generate data points (f,d,c)
        f=sf.freq;
        for f_ind=1:length(f)
            myf=f(f_ind);
%             disp(myf)
            if myf>60,break,end
            for ch1=1:max_electrode-1
                for ch2=ch1+1:max_electrode
                    data_collection_counter = data_collection_counter + 1;
                    v(data_collection_counter,:)=[myf,d(ch1,ch2),sf.Cxy(ch1,ch2).mag(f_ind)];
                end
            end
        end

        % iter over freq bins
        for freqbin=1:length(f_hi)
            disp(f_bin_names{freqbin})
            myf_low=f_low(freqbin);
            myf_hi=f_hi(freqbin);

            temp=zeros(size(v));
            counter=0;
            for n=counter_file_start:data_collection_counter
                % check lower bound of freq
                if v(n,1)>myf_low
                    % check upper bound of freq
                    if v(n,1)>myf_hi
                        break;
                    end
                    counter=counter+1;
                    % append data point to temp matrix
                    temp(counter,:)=v(n,:);
                end
            end
            array =  temp(1:counter,2:3);
            [C,ia,idx] = unique(array(:,1),'stable');
            val = accumarray(idx,array(:,2),[],@mean); 
            your_mat = [C val];
%             subplot(1,length(f_hi),freqbin)
%             plot(your_mat(:,1),your_mat(:,2),'*');title(f_bin_names{freqbin})
%             ylim([0 1]),xlabel 'Distance [mm]',ylabel 'Coherence'
        %         figure;y=interp1(your_mat(:,1),your_mat(:,2),0:.5:80);figure
        %         plot(0:.5:80,y);title(['the data',num2str(freqbin)])
            P = polyfit(your_mat(:,1),your_mat(:,2),1);
%             yfit = P(1)*your_mat(:,1)+P(2);
            slope(str2num(daynum)-1,freqbin)=P(1);
            yintercept(str2num(daynum)-1,freqbin)=P(2);
            datamean(str2num(daynum)-1,freqbin)=mean(array(:,2));
        end
    
    end
    if plot_example
        %plot example coherence graphs
        figure(coh_ex);
        ch1=10;ch2=60;
%         if ch1>=ch2,continue,end%data is not duplicated across diagonal
        subplot(7,1,subplotnum),subplotnum=subplotnum+1;
        plot(sf.freq,sf.Cxy(ch1,ch2).mag)
        ylim([0 1])
        xlim([0 50])
%         if daynum~='8'
%             set(gca,'XTick',[]);
%         end
        if daynum=='8'
            xlabel('Frequency [Hz]')
        end
        if daynum=='5'
            ylabel 'Mean Square Coherence'
        end
        titl=['Day ',daynum];
%         title(['MSC channels ',num2str(ch1),' and ',num2str(ch2),', ',titl])
        title(titl)
    end
    if one_day_only,break,end
end
temp=repmat(f_bin_names,size(slope,1),1);
figure;boxplot(yintercept(:),temp(:));ylabel 'Y Intercept Coherence'
figure
subplot(2,1,1);boxplot(datamean(:),temp(:));ylabel 'Mean Coherence'
subplot(2,1,2);boxplot(slope(:),temp(:));ylabel 'Slope Coherence/Distance [1/mm]'
slope_sig=ttest_all_columns(slope);
yintercept_sig=ttest_all_columns(yintercept);
datamean_sig=ttest_all_columns(datamean);
disp(slope_sig)
disp(yintercept_sig)
disp(datamean_sig)
disp(counter);
xq=0:.5:80;
yq=0:.5:80;
if plot_surf
    figure;vq=griddata(v(:,1),v(:,2),v(:,3),xq,xq');
    surf(xq,xq',vq);
end
% iter over data
slope=zeros(size(f_hi));
yintercept=zeros(size(f_hi));
figure
for freqbin=1:length(f_hi)
    disp(f_bin_names{freqbin})
    myf_low=f_low(freqbin);
    myf_hi=f_hi(freqbin);

    temp=zeros(size(v));
    counter=0;
    for n=1:size(v,1)
        % check lower bound of freq
        if v(n,1)>myf_low
            % check upper bound of freq
            if v(n,1)>myf_hi
                break;
            end
            counter=counter+1;
            % append data point to temp matrix
            temp(counter,:)=v(n,:);
        end
    end

    temp(counter+1:end,:)=[];

    array =  temp(:,2:3);
    [C,ia,idx] = unique(array(:,1),'stable');
    val = accumarray(idx,array(:,2),[],@mean); 
    your_mat = [C val];
    subplot(1,length(f_hi),freqbin)
    plot(your_mat(:,1),your_mat(:,2),'*');title(f_bin_names{freqbin})
    ylim([0 1])
    if freqbin==1
        ylabel 'Coherence'
    end
    if freqbin==3
        xlabel 'Distance [mm]'
    end
%         figure;y=interp1(your_mat(:,1),your_mat(:,2),0:.5:80);figure
%         plot(0:.5:80,y);title(['the data',num2str(freqbin)])
    P = polyfit(your_mat(:,1),your_mat(:,2),1);
    yfit = P(1)*your_mat(:,1)+P(2);
    hold on;
    plot(your_mat(:,1),yfit,'r-.');
    slope(freqbin)=P(1);
    yintercept(freqbin)=P(2);
end
temp=repmat(f_bin_names,size(slope,1),1);
figure;plot(slope);title 'slope'
figure;plot(yintercept);title 'y intercept'

    

function myslice(v)%v in (f,d,c)
figure;plot(v(:,2),v(:,3),'*');title('d vs c')
xlabel('d')
ylabel('c')
figure;interp1(v(:,2),v(:,3),0:.5:80)

end

function d=mydist(ch1,ch2,Grid)
    %calculate distance between two electrodes here
    %dummy var
    d=sqrt((Grid(ch1,1)-Grid(ch2,1))^2 + (Grid(ch1,2)-Grid(ch2,2))^2 + (Grid(ch1,3)-Grid(ch2,3))^2);
end

function m=ttest_all_columns(in)
    %calculate ttest2 on all pairs of columns
    m=zeros(size(in,2));
    for n=1:size(in,2)-1
        for nn=n+1:size(in,2)
            m(n,nn)=ttest2(in(:,n),in(:,nn));
            m(nn,n)=m(n,nn);
        end
    end
end