% function CalcCoherenceConditioning(name,sessions)
%% Calculate Coherence for each file in directory
clearvars;close all
tic
basedir = 'C:\Users\Yazdan Lab\Documents\MATLAB\Devon\BingData\c9';
cd(basedir);

%TODO: update the code to print only relevant frequency when the
%frequencies do not match

%iterate through the .edf files
ls=dir();
parfor filenum=1:length(ls)
% for filenum=1:length(ls)
    if isempty(strfind(ls(filenum).name,'.edf'))
        continue
    end
    filename=ls(filenum).name;
    
    %run body of parfor as a separate function to avoid parfor errors
    parfor_helper(filename);
    
end %parfor filenum=1:length(ls)
toc

function parfor_helper(filename)
    disp(['Running file: ',filename])
    [header,~]=edfread(filename,'targetSignals',1);

    samp_freq = mode(header.frequency);
    numchnls=length(header.frequency);
    
    % calculate cross spectrum
    Nwin = round(10*samp_freq);
    ovlp = round(Nwin/2);
    Nfft = 2^nextpow2(3000);

    Cxy(1:numchnls,1:numchnls) = struct(...
        'mag',[],...
        'phase',[]);

    for ch1 = 1:numchnls-1
        [header1,sig1]=edfread(filename,'targetSignals',ch1);
        for ch2 = ch1+1:numchnls
            [header2,sig2]=edfread(filename,'targetSignals',ch2);
            
            if ~isequal(header1.frequency(ch1),samp_freq) || ~isequal(header2.frequency(ch2),samp_freq)
                disp('Sampling frequencies are not equal. Skipping msc for:')
                disp(['Channel pair ',num2str(ch1),'(',num2str(header1.frequency),') - ',num2str(ch2),'(',num2str(header2.frequency),')'])
                %disp(['Channel pair ',num2str(ch1),'(',num2str(header1.frequency(ch1)),') - ',num2str(ch2),'(',num2str(header2.frequency(ch2)),')'])
                Cxy(ch1,ch2).mag = [];
                Cxy(ch1,ch2).phase = [];
                continue
            end
                
            [coherency,~] = cpsd(sig1,sig2,hamming(Nwin),ovlp,Nfft,samp_freq);
            [coherence,F] = mscohere(sig1,sig2,hamming(Nwin),ovlp,Nfft,samp_freq);

            % store variables
            Cxy(ch1,ch2).mag = coherence;
            Cxy(ch1,ch2).phase = angle(coherency);

            % output progress
            disp(['Working file: ',filename])
            msg = sprintf('ch%02i <--> ch%02i',ch1,ch2);
            fprintf('%s',msg)

        end
    end
    fprintf('\n')

    sf=struct();
    sf.Cxy = Cxy;
    sf.freq = F;
    sf.filename = filename;
    save(['coh_',filename(1:end-4),'.mat'], 'sf')
end
